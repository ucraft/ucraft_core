# tocraft_core
Plugin core serwera ToCraft.pl

## Aktualne możliwości pluginu
- Prosty system ekonomii, wraz z komendami na przelewy, dodawanie pieniedzy itp.
- Podstawowe komendy
- Warpy
- Kupowanie/sprzedawanie expa
- Customowy czat
- API do rejestracji komend bez uzycia plugin.yml (autorstwa TheMolkaPL)
- API do czatu (autorstwa MrGregorix)
- Tablista (wraz z API autorstwa NorthPL)

## Pluginy wymagane do działania:
- Vault
- ProtocolLib

## Lista autorow:
- @artur9010
- @MrGregorix
- @XopyIP
- Jezeli edytujesz kod, mozesz spokojnie dopisac sie do tej listy

### http://tocraft.pl/