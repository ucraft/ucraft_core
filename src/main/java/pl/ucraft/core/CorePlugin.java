package pl.ucraft.core;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.JavaPlugin;
import pl.themolka.cmds.command.Commands;
import pl.ucraft.core.commands.ChatCommand;
import pl.ucraft.core.commands.DropCommand;
import pl.ucraft.core.commands.HelpCommand;
import pl.ucraft.core.commands.NightVisionCommand;
import pl.ucraft.core.commands.SetSpawnCommand;
import pl.ucraft.core.commands.SpawnCommand;
import pl.ucraft.core.listeners.ChatListener;
import pl.ucraft.core.listeners.DropListener;
import pl.ucraft.core.listeners.EntityDeathListener;
import pl.ucraft.core.listeners.PlayerDeathListener;
import pl.ucraft.core.listeners.PlayerJoinListener;
import pl.ucraft.core.listeners.PlayerPortalListener;
import pl.ucraft.core.listeners.PlayerQuitListener;

import java.io.IOException;
import java.util.*;

public class CorePlugin extends JavaPlugin {

    public static Permission permission = null;
    public static Economy economy = null;
    public static Chat chat = null;
    private static CorePlugin plugin;
    private List<Material> durabilityFixAppliedItems = new ArrayList<>();
    private List<Material> durabilityFixSwords = new ArrayList<>();
    private boolean chatLocked;

    public void onEnable() {
        //Plugin
        plugin = this;
        chatLocked = false;

        //Register configs
        ConfigManager.registerConfig("spawn", "spawn.yml", this);
        ConfigManager.registerConfig("drop", "drop.yml", this);
        ConfigManager.loadAll();
        ConfigManager.saveAll();

        //Vault
        setupChat(); //Chat support
        setupEconomy(); //Economy support?
        setupPermissions(); //Permissions support


        //Register listeners
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new DropListener(), this);
        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(new PlayerDeathListener(), this);
        pm.registerEvents(new EntityDeathListener(), this);
        pm.registerEvents(new PlayerPortalListener(), this);
        //Register commands
        Commands.register(this, SpawnCommand.class);
        Commands.register(this, SetSpawnCommand.class);
        Commands.register(this, DropCommand.class);
        Commands.register(this, ChatCommand.class);
        Commands.register(this, NightVisionCommand.class);
        Bukkit.getServer().getPluginCommand("help").setExecutor(new HelpCommand());

        //CorePlayer things
        CorePlayer.load(this);

        //Drop things
        durabilityFixAppliedItems.add(Material.WOOD_SWORD);
        durabilityFixAppliedItems.add(Material.WOOD_PICKAXE);
        durabilityFixAppliedItems.add(Material.WOOD_AXE);
        durabilityFixAppliedItems.add(Material.WOOD_SPADE);
        durabilityFixAppliedItems.add(Material.STONE_SWORD);
        durabilityFixAppliedItems.add(Material.STONE_PICKAXE);
        durabilityFixAppliedItems.add(Material.STONE_AXE);
        durabilityFixAppliedItems.add(Material.STONE_SPADE);
        durabilityFixAppliedItems.add(Material.IRON_SWORD);
        durabilityFixAppliedItems.add(Material.IRON_PICKAXE);
        durabilityFixAppliedItems.add(Material.IRON_AXE);
        durabilityFixAppliedItems.add(Material.IRON_SPADE);
        durabilityFixAppliedItems.add(Material.GOLD_SWORD);
        durabilityFixAppliedItems.add(Material.GOLD_PICKAXE);
        durabilityFixAppliedItems.add(Material.GOLD_AXE);
        durabilityFixAppliedItems.add(Material.GOLD_SPADE);
        durabilityFixAppliedItems.add(Material.DIAMOND_SWORD);
        durabilityFixAppliedItems.add(Material.DIAMOND_PICKAXE);
        durabilityFixAppliedItems.add(Material.DIAMOND_AXE);
        durabilityFixAppliedItems.add(Material.DIAMOND_SPADE);
        durabilityFixAppliedItems.add(Material.SHEARS);
        durabilityFixSwords.add(Material.WOOD_SWORD);
        durabilityFixSwords.add(Material.STONE_SWORD);
        durabilityFixSwords.add(Material.IRON_SWORD);
        durabilityFixSwords.add(Material.GOLD_SWORD);
        durabilityFixSwords.add(Material.DIAMOND_SWORD);
    }

    public void onDisable() {
        ConfigManager.saveAll();
        CorePlayer.save(this);
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static String getTag() {
        return ChatColor.GREEN + getUncolouredTag() + ChatColor.RESET;
    }

    public static String getUncolouredTag() {
        return "[uCraft.pl] ";
    }

    public static Location getSpawnLocation() {
        ConfigManager.RConfig spawnCfg = ConfigManager.getConfig("spawn");
        if ((spawnCfg != null ? spawnCfg.getString("world") : null) == null)
            return null;
        Location spawn = new Location(null, 0, 0, 0);
        spawn.setX(spawnCfg.getDouble("x"));
        spawn.setY(spawnCfg.getDouble("y"));
        spawn.setZ(spawnCfg.getDouble("z"));
        spawn.setWorld(Bukkit.getWorld(spawnCfg.getString("world")));
        spawn.setYaw(spawnCfg.getInt("yaw"));
        spawn.setPitch(spawnCfg.getInt("pitch"));
        return spawn;
    }

    public static void setSpawnLocation(String world, double x, double y, double z, float yaw, float pitch) throws NullPointerException {
        ConfigManager.RConfig spawnCfg = ConfigManager.getConfig("spawn");
        assert spawnCfg != null;
        spawnCfg.set("world", world);
        spawnCfg.set("x", x);
        spawnCfg.set("y", y);
        spawnCfg.set("z", z);
        spawnCfg.set("yaw", yaw);
        spawnCfg.set("pitch", pitch);
        try {
            spawnCfg.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int randomAmount(int minAmount, int maxAmount) {
        return (int) Math.round(Math.random() * (maxAmount - minAmount) + minAmount);
    }

    public static void givePlayerDrop(Collection<ItemStack> items, Player player) {
        if (items != null) {
            ItemStack[] itemBoard = new ItemStack[items.size()];
            for (int i = 0; i < items.size(); i++) {
                itemBoard[i] = ((ArrayList<ItemStack>) items).get(i);
            }
            for(ItemStack item : player.getInventory().addItem(itemBoard).values()){
                player.getWorld().dropItemNaturally(player.getLocation(), item);
                //p.getWorld().dropItem(new Location(player.getWorld(), player.getLocation().getX() - 1, player.getLocation().getY(), p.getLocation().getZ()), stack);
            }
                //player.getInventory().addItem(itemBoard);
        }
    }

    public static void givePlayerExp(Player player, int exp){
        double half = 0.5 * exp;
        int halfInt = (int) half;
        player.giveExp(halfInt);
        Location locationAbovePlayer = player.getLocation().add(0, 1, 0);
        player.getWorld().spawn(locationAbovePlayer, ExperienceOrb.class).setExperience(halfInt);
    }

    public static void recalculateDurability(Player player) {
        ItemStack item = player.getItemInHand();
        int enchantLevel = item.getEnchantmentLevel(Enchantment.DURABILITY);
        int random = randomAmount(0, 100);
        if ((plugin.durabilityFixAppliedItems.contains(item.getType())) && ((enchantLevel == 0) || (random <= 100 / (enchantLevel + 1)))) {
            if (plugin.durabilityFixSwords.contains(item.getType())) {
                if (item.getDurability() + 2 >= item.getType().getMaxDurability()) {
                    player.getInventory().clear(player.getInventory().getHeldItemSlot());
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.0F);
                } else {
                    item.setDurability((short) (item.getDurability() + 2));
                }
            } else if (item.getDurability() + 1 >= item.getType().getMaxDurability()) {
                player.getInventory().clear(player.getInventory().getHeldItemSlot());
                player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.0F);
            } else {
                item.setDurability((short) (item.getDurability() + 1));
            }
            player.updateInventory();
        }
    }

    public static ItemStack fixLeavesDrop(ItemStack drop) {
        // Stare drzewa XD (te sprzed 1.7 czy tam 1.8)
        if (drop.getType().equals(Material.LEAVES)) {
            // Oak leaves
            if (drop.getDurability() == 4 || drop.getDurability() == 8 || drop.getDurability() == 12 || drop.getDurability() == 16 || drop.getDurability() == 20 || drop.getDurability() == 24) {
                return new ItemStack(Material.LEAVES, 1, (short) 0);
            }

            // Spruce leaves
            if (drop.getDurability() == 5 || drop.getDurability() == 9 || drop.getDurability() == 13 || drop.getDurability() == 17 || drop.getDurability() == 21 || drop.getDurability() == 25) {
                return new ItemStack(Material.LEAVES, 1, (short) 1);
            }

            // Birch leaves
            if (drop.getDurability() == 6 || drop.getDurability() == 10 || drop.getDurability() == 18 || drop.getDurability() == 14 || drop.getDurability() == 22 || drop.getDurability() == 26) {
                return new ItemStack(Material.LEAVES, 1, (short) 2);
            }

            // Jungle leaves
            if (drop.getDurability() == 7 || drop.getDurability() == 11 || drop.getDurability() == 15 || drop.getDurability() == 19 || drop.getDurability() == 23 || drop.getDurability() == 27) {
                return new ItemStack(Material.LEAVES, 1, (short) 3);
            }
        }
        // Te gowna co mojang dodal w jakis tam wersjach...
        if (drop.getType().equals(Material.LEAVES_2)) {
            // Akacja
            if (drop.getDurability() == 4 || drop.getDurability() == 8 || drop.getDurability() == 12 || drop.getDurability() == 16) {
                return new ItemStack(Material.LEAVES_2, 1, (short) 0);
            }
            // Dark oak
            if (drop.getDurability() == 5 || drop.getDurability() == 9 || drop.getDurability() == 13 || drop.getDurability() == 17) {
                return new ItemStack(Material.LEAVES_2, 1, (short) 1);
            }

        }

        //If shit, return shit :>
        return new ItemStack(Material.STONE);
    }

    @SuppressWarnings("ConstantConditions")
    public static double getDropChance(String item) {
        return ConfigManager.getConfig("drop").getDouble(item);
    }

    public static boolean isChatLocked() {
        return plugin.chatLocked;
    }

    public static void toogleChat() {
        if (plugin.chatLocked) {
            broadcast("&7Czat zostal odblokowany.");
            plugin.chatLocked = false;
        } else {
            broadcast("&7Czat zostal zablokowany.");
            plugin.chatLocked = true;
        }
    }

    public static void sendPrefixed(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', CorePlugin.getTag() + message));
    }

    public static void send(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static void sendHeader(CommandSender sender, String message){
        send(sender, "&8&m----------[&r &2&l" + message + " &8&m]----------");
    }

    public static void sendCommandHelp(CommandSender sender, String command, String description){
        send(sender, "&3/" + command + " &8- &7" + description);
    }

    public static void broadcastPrefixed(String message){
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', CorePlugin.getTag() + message));
    }

    public static void broadcast(String message){
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
}

