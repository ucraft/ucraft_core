package pl.ucraft.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.Plugin;

public class CorePlayer implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Map<UUID, CorePlayer> players;
    private UUID uuid;
    private int dropTier = 1;
    private int dropExp = 0;

    public CorePlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUUID(){
        return uuid;
    }

    public int getDropTier(){
        return dropTier;
    }

    public int getDropExp(){
        return dropExp;
    }

    public boolean canTierLevelUp(){
        switch(getDropTier()){
            case 1:
                return getDropExp() > 1000;
            case 2:
                return getDropExp() > 4000;
            case 3:
                return getDropExp() > 15000;
            case 4:
                return getDropExp() > 30000;
            default:
                return false;
        }
    }

    public boolean tierLevelUp(){
        if(canTierLevelUp()){
            setDropTier(getDropTier() + 1);
            return true;
        }
        return false;
    }

    public void setDropTier(int tier){
        dropTier = tier;
    }

    public void setDropExp(int exp){
        dropExp = exp;
    }

    public void addDropExp(int toAdd){
        dropExp = dropExp + toAdd;
    }

    public static void load(Plugin p) {
        players = null;
        if (new File(p.getDataFolder(), "players.sav").exists())
            players = load(new File(p.getDataFolder(), "players.sav"));

        if (players == null)
            players = new HashMap<>();
    }

    public static void save(Plugin p) {
        save(players, new File(p.getDataFolder(), "players.sav"));
    }

    private static void save(Object map, File f) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(f));
            oos.writeObject(map);
            oos.flush();
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> T load(File f) {
        try {
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object result = null;
            if (fis.available() > 0)
                result = ois.readObject();
            ois.close();
            return (T) result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static CorePlayer getCorePlayer(UUID uuid){
        return players.computeIfAbsent(uuid, k -> new CorePlayer(uuid));
        /*
        CorePlayer player = players.get(uuid);

        if(player == null){
            player = new CorePlayer(uuid);
            players.put(uuid, player);
        }
        return player;
         */
    }
}
