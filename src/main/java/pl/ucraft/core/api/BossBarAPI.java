package pl.ucraft.core.api;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class BossBarAPI {

    private static Map<Player, BossBar> playerBars = new HashMap<>();

    /**
     * Creates boss bar and displays it to player.
     * @param player player to show bossbar
     * @param text text to be shown on bossbar
     * @param value bossbar value
     * @param color bossbar color
     * @param style bossbar style
     * @param flag bossbar flag
     */
    public static void setBossBar(Player player, String text, double value, BarColor color, BarStyle style, BarFlag flag){
        if(color == null){
            color = BarColor.PURPLE;
        }
        if(style == null){
            style = BarStyle.SOLID;
        }
        if(playerBars.containsKey(player)){
            BossBar bar = getPlayerBossBar(player);
            bar.setProgress(value);
            bar.setColor(color);
            bar.setStyle(style);
            bar.setTitle(text);
        }else{
            BossBar bar = Bukkit.createBossBar(text, color , style);
            bar.setProgress(value);
            if(flag != null){
                bar.addFlag(flag);
            }
            bar.addPlayer(player);
            bar.setVisible(true);
            playerBars.put(player, bar);
        }
    }

    /**
     * Removes all bossbars from the player.
     * @param player player to remove bossbar
     */
    public static void removeBossBar(Player player){
        if(playerBars.containsKey(player)){
            BossBar bar = getPlayerBossBar(player);
            bar.removeAll();
            playerBars.remove(player);
        }
    }

    /**
     * Get player current bossbar
     * @param player player
     * @return BossBar
     */
    public static BossBar getPlayerBossBar(Player player){
        if(playerBars.containsKey(player)){
            return playerBars.get(player);
        }
        return null;
    }
}
