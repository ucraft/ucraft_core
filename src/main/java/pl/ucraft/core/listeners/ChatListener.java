package pl.ucraft.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import pl.ucraft.core.CorePlugin;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        if(event.isCancelled()){
            return;
        }
        event.setCancelled(true);
        if (CorePlugin.isChatLocked()) {
            if (!event.getPlayer().hasPermission("ucraft.chat.bypass")) {
                CorePlugin.send(event.getPlayer(), "&cCzat jest zablokowany.");
                return;
            }
        }
        if (event.getPlayer().hasPermission("ucraft.chat.colour")) {
            event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
        }

        String format = ChatColor.translateAlternateColorCodes('&', CorePlugin.chat.getPlayerPrefix(event.getPlayer())) +  event.getPlayer().getDisplayName() + ChatColor.WHITE + ": " + event.getMessage();
        Bukkit.getServer().broadcastMessage(format);
    }
}
