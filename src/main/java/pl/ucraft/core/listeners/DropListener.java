package pl.ucraft.core.listeners;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import pl.ucraft.core.CorePlayer;
import pl.ucraft.core.CorePlugin;

import java.util.Collection;

/*
Dzialanie systemu dropu:
POdzial dropu na "tiery"
Tier to taki jakby nasz poziom, od "tieru" zalezy nasz drop.

Dziękuje @XopyIP za pomysł:
XopyIP przed chwilą - XopyIP: np tier 1 - wegiel + iron
XopyIP przed chwilą - XopyIP: tier 2 gold + cos tam
XopyIP przed chwilą - XopyIP: i tak dalej :D
XopyIP przed chwilą - XopyIP: aby miec diaxa trzeba miec np tier 4 lub 5
XopyIP przed chwilą - XopyIP: wtedy aby bylo nas stac na jakies ity trzeba pograc troche a nie liczyc na szczescie :d
XopyIP przed chwilą - XopyIP: artur mozesz progressbara dodac :D
XopyIP przed chwilą - XopyIP: w tabie najlepiej
XopyIP przed chwilą - XopyIP: tymi pelnymi znaczkami
XopyIP przed chwilą - XopyIP: pokolorowanymi
XopyIP przed chwilą - XopyIP: zielone te co juz masz i czerwone ktorych brakuuje :d
XopyIP przed chwilą - XopyIP: i tak 4 kolumny xd
XopyIP przed chwilą - XopyIP: lub 2 w srodku co sa

Podzial:
Tier 1 - wegiel, zelazo
Tier 2 - zloto, lapis
Tier 3 - redstone
Tier 4 - emerald
Tier 5 - diamond, spawner

 */

public class DropListener implements Listener{
    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event){
        if(event.isCancelled()){
            return;
        }
        if(event.getPlayer().getGameMode().equals(GameMode.CREATIVE)){
            return;
        }
        // Plecaki
        // silver, black, blue, brown, cyan, gray, green, light blue, lime, magneta
        if(event.getBlock().getType().equals(Material.SILVER_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.BLACK_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.BLUE_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.BROWN_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.CYAN_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.GRAY_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.GREEN_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.LIGHT_BLUE_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.LIME_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.MAGENTA_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.ORANGE_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.PINK_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.PURPLE_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.RED_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.WHITE_SHULKER_BOX) ||
                event.getBlock().getType().equals(Material.YELLOW_SHULKER_BOX)){
            return;
        }
        // Uprawy
        if(event.getBlock().getType().equals(Material.WHEAT) ||
                event.getBlock().getType().equals(Material.SEEDS) ||
                event.getBlock().getType().equals(Material.NETHER_WARTS) ||
                event.getBlock().getType().equals(Material.MELON_STEM) ||
                event.getBlock().getType().equals(Material.PUMPKIN_STEM) ||
                event.getBlock().getType().equals(Material.CARROT) ||
                event.getBlock().getType().equals(Material.CROPS) ||
                event.getBlock().getType().equals(Material.POTATO) ||
                event.getBlock().getType().equals(Material.BEETROOT)){
            return;
        }
        // Glowy
        if(event.getBlock().getType().equals(Material.SKULL)){
            return;
        }

        Player player = event.getPlayer();
        Location locationAbovePlayer = event.getPlayer().getLocation().add(0, 1, 0);

        CorePlayer coreplayer = CorePlayer.getCorePlayer(player.getUniqueId());
        Block block = event.getBlock();

        Collection<ItemStack> drops = block.getDrops(player.getInventory().getItemInMainHand());
        //Collection<ItemStack> drops = block.getDrops(player.getItemInHand());

        if(block.getType().equals(Material.SNOW)){
            drops.add(new ItemStack(Material.SNOW_BALL));
        }

        if (block.getType().equals(Material.LEAVES) || block.getType().equals(Material.LEAVES_2)) {
            //if (event.getPlayer().getItemInHand().getType() == Material.SHEARS) {
            if (player.getInventory().getItemInMainHand().getType() == Material.SHEARS) {
                drops.add(CorePlugin.fixLeavesDrop(new ItemStack(block.getType(), 1, (short) block.getData())));
            }
        }

        // drop z duzej trawy
        if(block.getType().equals(Material.LONG_GRASS)){
            // nasiona dynii
            if(Math.random() * 100.0D <= 0.5){
                drops.add(new ItemStack(Material.PUMPKIN_SEEDS, 1));
            }
            // nasiona arbuza
            if(Math.random() * 100.0D <= 0.75){
                drops.add(new ItemStack(Material.MELON_SEEDS, 1));
            }
        }


        // drop spawneru
        //todo: silktouch
        //todo: drop spawner tego samego typu
        if(block.getType().equals(Material.MOB_SPAWNER)){
            drops.add(new ItemStack(Material.MOB_SPAWNER, 1));
        }

        if(block.getType().equals(Material.DIRT) || block.getType().equals(Material.GRASS)){
            if(block.getType().equals(Material.GRASS) && player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.SILK_TOUCH)){
                drops.clear();
                drops.add(new ItemStack(Material.GRASS, 1));
            }

        }

        // silktouch - melon
        if(block.getType().equals(Material.MELON_BLOCK) && player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.SILK_TOUCH)){
            drops.clear();
            drops.add(new ItemStack(Material.MELON_BLOCK, 1));
        }

        // drop z stone
        if(block.getType().equals(Material.STONE) || block.getType().equals(Material.COBBLESTONE)){
            if(block.getType().equals(Material.STONE) && player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.SILK_TOUCH)){
                drops.clear();
                drops.add(new ItemStack(Material.STONE, 1));
            }

            coreplayer.addDropExp(CorePlugin.randomAmount(0, 3));
            CorePlugin.economy.depositPlayer(player, 0.01);
            /* Wszystkie tiery */
            if(Math.random() * 100.0D <= CorePlugin.getDropChance("coal")){
                double fortuneRandom1 = Math.random() * 100.0D;
                double fortuneRandom2 = Math.random() * 100.0D;
                double fortuneRandom3 = Math.random() * 100.0D;
                ItemStack wegiel = new ItemStack(Material.COAL, 1);
                if ((30.0D <= fortuneRandom1) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                    wegiel.setAmount(2);
                } else if ((20.0D <= fortuneRandom2) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                    wegiel.setAmount(3);
                } else if ((10.0D <= fortuneRandom3) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                    wegiel.setAmount(4);
                }
                drops.add(wegiel);
                CorePlugin.send(player, "&7Znalazles &3" + wegiel.getAmount() + " wegiel&7!");
                coreplayer.addDropExp(CorePlugin.randomAmount(2, 9) * wegiel.getAmount());
                //event.getPlayer().getWorld().spawn(locationAbovePlayer, ExperienceOrb.class).setExperience(4);
                CorePlugin.givePlayerExp(player, 4);
            }
            if(Math.random() * 100.0D <= CorePlugin.getDropChance("iron")){
                drops.add(new ItemStack(Material.IRON_ORE));
                CorePlugin.send(player, "&7Znalazles &3zelazo&7!");
                coreplayer.addDropExp(CorePlugin.randomAmount(5, 21));
            }
            if(Math.random() * 100.0D <= CorePlugin.getDropChance("clay")){
                int amount = CorePlugin.randomAmount(1, 3);
                drops.add(new ItemStack(Material.CLAY_BALL, amount));
                CorePlugin.send(player, "&7Znalazles &3" + amount + " gliny&7!");
                coreplayer.addDropExp(CorePlugin.randomAmount(1, 3));
            }
            /* Od tieru 2 */
            if(coreplayer.getDropTier() >= 2){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("gold") && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.GOLD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.IRON_PICKAXE)){
                    drops.add(new ItemStack(Material.GOLD_ORE));
                    CorePlugin.send(player, "&7Znalazles &3zloto&7!");
                    coreplayer.addDropExp(CorePlugin.randomAmount(6, 24));
                }
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("lapis")){
                    int amount = CorePlugin.randomAmount(1, 6);
                    drops.add(new ItemStack(Material.INK_SACK, amount, (short) 4));
                    CorePlugin.send(player, "&7Znalazles &3" + amount + " lapis&7!");
                    coreplayer.addDropExp(CorePlugin.randomAmount(4, 7) * amount);
                    CorePlugin.givePlayerExp(player, 6);
                }
            }
            /* Od tieru 3 */
            if(coreplayer.getDropTier() >= 3){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("redstone") && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.GOLD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.IRON_PICKAXE)){
                    int amount = CorePlugin.randomAmount(1, 6);
                    drops.add(new ItemStack(Material.REDSTONE, amount));
                    CorePlugin.send(player, "&7Znalazles &3" + amount + " redstone&7!");
                    coreplayer.addDropExp(CorePlugin.randomAmount(4, 10) * amount);
                    CorePlugin.givePlayerExp(player, 6);
                }
            }
            /* Od tieru 4 */
            if(coreplayer.getDropTier() >= 4){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("emerald") && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.GOLD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.IRON_PICKAXE)){
                    double fortuneRandom1 = Math.random() * 100.0D;
                    double fortuneRandom2 = Math.random() * 100.0D;
                    double fortuneRandom3 = Math.random() * 100.0D;
                    ItemStack emerald = new ItemStack(Material.EMERALD, 1);
                    if ((30.0D <= fortuneRandom1) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 3));
                    } else if ((20.0D <= fortuneRandom2) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 4));
                    } else if ((10.0D <= fortuneRandom3) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 5));
                    }
                    drops.add(emerald);
                    if(emerald.getAmount() == 1){
                        CorePlugin.send(player, "&7Znalazles &3szmaragd&7!");
                    }else if((emerald.getAmount() >= 2) && (emerald.getAmount() <= 4)){
                        CorePlugin.send(player, "&7Znalazles &3" + emerald.getAmount() + " szmaragdy&7!");
                    }else{
                        CorePlugin.send(player, "&7Znalazles &3" + emerald.getAmount() + " szmaragdow&7!");
                    }
                    coreplayer.addDropExp(CorePlugin.randomAmount(7, 13) * emerald.getAmount());
                    CorePlugin.givePlayerExp(player, 6);
                }
            }
            /* Tier 5 - ostatni */
            if(coreplayer.getDropTier() >= 5){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("diamond") && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.GOLD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.IRON_PICKAXE)){
                    double fortuneRandom1 = Math.random() * 100.0D;
                    double fortuneRandom2 = Math.random() * 100.0D;
                    double fortuneRandom3 = Math.random() * 100.0D;
                    ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
                    if ((30.0D <= fortuneRandom1) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 3));
                    } else if ((20.0D <= fortuneRandom2) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 4));
                    } else if ((10.0D <= fortuneRandom3) && (player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 5));
                    }
                    drops.add(diamond);
                    if(diamond.getAmount() == 1){
                        CorePlugin.send(player, "&7Znalazles &3diament&7!");
                    }else if((diamond.getAmount() >= 2) && (diamond.getAmount() <= 4)){
                        CorePlugin.send(player, "&7Znalazles &3" + diamond.getAmount() + " diamenty&7!");
                    }else{
                        CorePlugin.send(player, "&7Znalazles &3" + diamond.getAmount() + " diamentow&7!");
                    }
                    CorePlugin.givePlayerExp(player, 6);
                    //coreplayer.addDropExp(CorePlugin.randomAmount(7, 17) * diamond.getAmount()); //ugh, we need drop exp at last tier?
                }

                // drop spawnerow ;>
                if(Math.random() * 100.0D <= 0.00001 && player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE){
                    coreplayer.addDropExp(CorePlugin.randomAmount(500, 1200));
                    event.getPlayer().getWorld().spawn(locationAbovePlayer, ExperienceOrb.class).setExperience(7500);
                    CorePlugin.broadcast("&3Gracz " + player.getName() + " znalazl spawner!");
                    drops.add(new ItemStack(Material.MOB_SPAWNER));
                }
            }

            if(coreplayer.getDropTier() != 5){
                // Meh, badziewny kod na sprawdzanie czy zlevelupowal xD
                if(coreplayer.tierLevelUp()){
                    CorePlugin.send(player, "&3Osiagnales nastepny tier.");
                }
            }

        }

        CorePlugin.givePlayerDrop(drops, player);
        CorePlugin.recalculateDurability(player);
        block.setType(Material.AIR);
    }
}
