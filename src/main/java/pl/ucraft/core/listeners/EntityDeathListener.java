package pl.ucraft.core.listeners;

import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import pl.ucraft.core.CorePlugin;

public class EntityDeathListener implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        //Player
        Player player = event.getEntity().getKiller();
        if (player != null) {
            CorePlugin.givePlayerDrop(event.getDrops(), player);
            CorePlugin.givePlayerExp(player, event.getDroppedExp());
            event.getDrops().clear();
            event.setDroppedExp(0);
            // Money drop :> from mobs
            switch (event.getEntityType()) {
                case ZOMBIE:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 5));
                    break;
                case SKELETON:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 7));
                    break;
                case SPIDER:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 4));
                    break;
                case CAVE_SPIDER:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 8));
                    break;
                case BAT:
                    CorePlugin.economy.depositPlayer(player, 0.5);
                    break;
                case GIANT:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(200, 500));
                    break;
                case SQUID:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(2, 10));
                    break;
                case GHAST:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(30, 70));
                    break;
                case HORSE:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(4, 11));
                    break;
                case BLAZE:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(5, 50));
                    break;
                case ENDER_DRAGON:
                    CorePlugin.economy.depositPlayer(player, 10000);
                    CorePlugin.broadcast("&7Gracz &3" + player.getName() + " &7pokonal smoka! Gratulacje!");
                    break;
                case WITHER:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(500, 1500));
                    break;
                case ENDERMITE:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(10, 30));
                    break;
                case GUARDIAN:
                    CorePlugin.economy.depositPlayer(player, 5000);
                    CorePlugin.broadcastPrefixed("&7Gracz &3" + player.getName() + " &7pokonal straznika! Gratulacje!");
                    break;
                case SNOWMAN:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(5, 10));
                    break;
                case MAGMA_CUBE:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(10, 30));
                    break;
                case WOLF:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 5));
                    break;
                case PIG:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 5));
                    break;
                case COW:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 8));
                    break;
                case MUSHROOM_COW:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(10, 50));
                    break;
                case SHEEP:
                    CorePlugin.economy.depositPlayer(player, CorePlugin.randomAmount(1, 5));
                    break;
            }
        }
    }

}
