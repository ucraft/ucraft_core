package pl.ucraft.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        event.setDeathMessage(null); //Remove death message
        event.setDroppedExp(0); //Remove dropped exp
        event.setKeepLevel(true); //Keep level
    }
}
