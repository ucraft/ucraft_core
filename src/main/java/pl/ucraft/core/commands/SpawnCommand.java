package pl.ucraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

public class SpawnCommand extends Command {
    public SpawnCommand() {
        super(new String[]{"spawn", "sapwn", "spanw"});
        super.setDescription("Teleportacja na spawn");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        if (CorePlugin.getSpawnLocation() == null)
            throw new CommandException(ChatColor.RED + "Spawn nie jest ustawiony!");

        player.teleport(CorePlugin.getSpawnLocation());
    }
}
