package pl.ucraft.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 29.12.2016.
 */
public class HelpCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length == 0){
            CorePlugin.sendHeader(commandSender, "Pomoc");
            CorePlugin.sendCommandHelp(commandSender, "help wyspy", "Pomoc dotyczaca wysp.");
            CorePlugin.sendCommandHelp(commandSender, "help targ", "Pomoc dotyczaca targu online.");
            CorePlugin.sendCommandHelp(commandSender, "help pieniadze", "Pomoc dotyczaca pieniedzy.");
            CorePlugin.sendCommandHelp(commandSender, "help rangi", "Pomoc dotyczaca rang na serwerze.");
            CorePlugin.sendCommandHelp(commandSender, "spawn", "Teleport na spawn.");
            CorePlugin.sendCommandHelp(commandSender, "drop", "Informacje dotyczace dropu z kamienia.");
            CorePlugin.sendCommandHelp(commandSender, "vip", "Informacje dotyczace rangi VIP.");
            CorePlugin.sendCommandHelp(commandSender, "msg", "Wiadomosc prywatna.");
            CorePlugin.sendCommandHelp(commandSender, "helpop", "Pytanie do administracji.");
        }else{
            if(strings[0].equalsIgnoreCase("wyspy")){
                CorePlugin.sendHeader(commandSender, "Pomoc dot. wysp");
                CorePlugin.sendCommandHelp(commandSender, "nowa", "Utworz nowa wyspe.");
                CorePlugin.sendCommandHelp(commandSender, "usun", "Usun wyspe.");
                CorePlugin.sendCommandHelp(commandSender, "dom", "Przeteleportuj sie na wyspe.");
                CorePlugin.sendCommandHelp(commandSender, "sethome", "Ustawia nowe polozenie /dom");
                CorePlugin.sendCommandHelp(commandSender, "dodaj", "Dodaje kolege do wyspy.");
                CorePlugin.sendCommandHelp(commandSender, "opusc", "Opusc party.");
                CorePlugin.sendCommandHelp(commandSender, "zadania", "Spis dostepnych zadan.");
                CorePlugin.sendCommandHelp(commandSender, "ukoncz", "Ukoncz zadanie.");
                CorePlugin.sendCommandHelp(commandSender, "island", "Pelny spis komend dot. wysp.");
            }else if(strings[0].equalsIgnoreCase("targ")){
                CorePlugin.sendHeader(commandSender, "Pomoc dot. targu");
                CorePlugin.send(commandSender, "&7Aby sprzedawac/kupowac na targu pierw musisz sie na nim zarejestrowac. Wpisz &3/targ haslo &7by zdobyc haslo" +
                        "&7a nastepnie wejdz na &3ucraft.pl/targ &7by zalogowac sie (login to nick z gry), jesli chcesz cos sprzedac wloz przedmiot do" +
                        "&3/targ skrytka &7a potem wejdz do zakladki &3Moje przedmioty &7na targu, tam mozesz je wystawic. Kupione przedmioty odbierasz" +
                        "&7w skrytce.");
            }else if(strings[0].equalsIgnoreCase("pieniadze")){
                CorePlugin.sendHeader(commandSender, "Pomoc dot. pieniedzy");
                CorePlugin.sendCommandHelp(commandSender, "money", "Stan konta.");
                CorePlugin.sendCommandHelp(commandSender, "pay <nick> <ilosc>", "Przelej pieniadze innemu graczowi.");
            }else if(strings[0].equalsIgnoreCase("rangi")){
                CorePlugin.sendHeader(commandSender, "Pomoc dot. rang");
                CorePlugin.send(commandSender, "&7Rangi na serwerze sa oznaczone kolorami:");
                CorePlugin.send(commandSender, "&3Tym kolorem oznaczony jest Wlasciciel serwera, on ma 100% wladze na tym serwerze.");
                CorePlugin.send(commandSender, "&cKolorem czerwonym oznaczeni sa Administratorzy, zajmuja sie oni pomaganiem w prowadzeniu serwera.");
                CorePlugin.send(commandSender, "&5Ciemno-fioletowym za to oznaczamy Moderatorow, pilnuja oni porzadku na serwerze.");
                CorePlugin.send(commandSender, "&9Na jasny niebieski zasluguja Pomocnicy, ich zadaniem jest pomaganie innym.");
                CorePlugin.send(commandSender, "&6Kolorem zlotym oznaczeni sa gracze ktorzy zakupili range VIP.");
                CorePlugin.send(commandSender, "&7Kolor szary jest dla zwyklych szaraczkow, czyli zwyklych szarych graczy :)");
            }
        }
        return true;
    }
}
