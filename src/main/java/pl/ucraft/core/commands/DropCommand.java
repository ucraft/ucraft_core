package pl.ucraft.core.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlayer;
import pl.ucraft.core.CorePlugin;

public class DropCommand extends Command {

    public DropCommand() {
        super(new String[]{"drop"});
        super.setDescription("Informacje na temat dropu ze stone");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        CorePlayer player = CorePlayer.getCorePlayer(((Player) sender).getUniqueId());
        if(player.getDropTier() != 5){ //wyswietlaj te informacje tylko na 1-4
            CorePlugin.send(sender, "&7Drop jest oparty na \"tierach\", aby zdobyc nastepny \"tier\" nalezy zdobyc dana ilosc punktow doswiadczenia.");
            CorePlugin.send(sender, "&7Twoj tier to &3" + player.getDropTier());
            CorePlugin.send(sender, "&7Posiadasz &3" + player.getDropExp() + "&7/&3" + expToLevel(player.getDropTier()) + " &7punktow doswiadczenia.");
        }
        CorePlugin.send(sender, "&3Drop: &7(przedmioty wypadaja z kamienia)");
        if(player.getDropTier() == 5){
            CorePlugin.send(sender, "&a[Tier 5] &7Spawner: &30.00001% &7(wymagany kilof diamentowy)");
            CorePlugin.send(sender, "&a[Tier 5] &7Diament: &3" + CorePlugin.getDropChance("diamond") + "% &7(wymagany kilof zelazny)");
        }else{
            CorePlugin.send(sender, "&c[Tier 5] &7Spawner: &30.00001% &7(wymagany kilof diamentowy)");
            CorePlugin.send(sender, "&c[Tier 5] &7Diament: &3" + CorePlugin.getDropChance("diamond") + "% &7(wymagany kilof zelazny)");
        }
        if(player.getDropTier() >= 4){
            CorePlugin.send(sender, "&a[Tier 4] &7Szmaragd: &3" + CorePlugin.getDropChance("emerald") + "% &7(wymagany kilof zelazny)");
        }else{
            CorePlugin.send(sender, "&c[Tier 4] &7Szmaragd: &3" + CorePlugin.getDropChance("emerald") + "% &7(wymagany kilof zelazny)");
        }
        if(player.getDropTier() >= 3){
            CorePlugin.send(sender, "&a[Tier 3] &7Redstone: &3" + CorePlugin.getDropChance("redstone") + "% &7(wymagany kilof zelazny)");
        }else{
            CorePlugin.send(sender, "&c[Tier 3] &7Redstone: &3" + CorePlugin.getDropChance("redstone") + "% &7(wymagany kilof zelazny)");
        }
        if(player.getDropTier() >= 2){
            CorePlugin.send(sender, "&a[Tier 2] &7Zloto: &3" + CorePlugin.getDropChance("gold") + "% &7(wymagany kilof zelazny)");
            CorePlugin.send(sender, "&a[Tier 2] &7Lapis: &3" + CorePlugin.getDropChance("lapis") + "%");
        }else{
            CorePlugin.send(sender, "&c[Tier 2] &7Zloto: &3" + CorePlugin.getDropChance("gold") + "% &7(wymagany kilof zelazny)");
            CorePlugin.send(sender, "&c[Tier 2] &7Lapis: &3" + CorePlugin.getDropChance("lapis") + "%");
        }
        CorePlugin.send(sender, "&a[Tier 1] &7Glina: &3" + CorePlugin.getDropChance("clay") + "%");
        CorePlugin.send(sender, "&a[Tier 1] &7Zelazo: &3" + CorePlugin.getDropChance("iron") + "%");
        CorePlugin.send(sender, "&a[Tier 1] &7Wegiel: &3" + CorePlugin.getDropChance("coal") + "%");
    }

    private int expToLevel(int level){
        switch(level){
            case 1:
                return 1000;
            case 2:
                return 4000;
            case 3:
                return 15000;
            case 4:
                return 30000;
            default:
                return 0;
        }
    }

}
