package pl.ucraft.core.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

public class NightVisionCommand extends Command {

    public NightVisionCommand() {
        super(new String[]{"nightvision", "nv"});
        super.setDescription("Wlacz/wylacz widzenie w ciemnosci.");
        super.setPermission("ucraft.core.nightvision");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player)sender;
        if(player.hasPotionEffect(PotionEffectType.NIGHT_VISION)){
            CorePlugin.send(player, "&7Wylaczono widzenie w ciemnosci.");
            player.removePotionEffect(PotionEffectType.NIGHT_VISION);
        }else{
            PotionEffect effect = new PotionEffect(PotionEffectType.NIGHT_VISION, 999999 * 20, 2);
            CorePlugin.send(player, "&7Wlaczono widzenie w ciemnosci.");
            player.addPotionEffect(effect);
        }
    }

}
