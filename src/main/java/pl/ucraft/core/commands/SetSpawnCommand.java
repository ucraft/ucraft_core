package pl.ucraft.core.commands;

import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

public class SetSpawnCommand extends Command {
    public SetSpawnCommand() {
        super(new String[]{"setspawn"});
        super.setDescription("Ustaw spawn");
        super.setPermission("ucraft.core.setspawn");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        CorePlugin.setSpawnLocation(player.getWorld().getName(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
        CorePlugin.send(player, "&7Spawn zostal ustawiony.");
    }
}
